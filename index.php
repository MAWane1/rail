<?php
/*
 * Copyright 2015-2016 Mark Wane
 *
 * This file is part of Rail rovers and rangers comparison tool.
 *
 * Rail rovers and rangers comparison tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rail rovers and rangers comparison tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Rail rovers and rangers comparison tool.  If not, see <http://www.gnu.org/licenses/>.
 */
 
session_start();
header("Content-Security-Policy: default-src 'none' ; script-src 'self' ; style-src 'self' https://fonts.googleapis.com; font-src https://fonts.gstatic.com; form-action 'self'; upgrade-insecure-requests; block-all-mixed-content; report-uri https://cool110.report-uri.io/r/default/csp/enforce;");
require_once("fare_access.php");
require_once("display.php");
if(empty($_SESSION["journeys"])){ // Initialise new session
	$_SESSION["journeys"] = array();
	$_SESSION["stations"] = array();
	$_SESSION["undo"] = array();
}

/**
 * Performs basic sanitisation of input for API calls
 *
 * @param string $data input is sanitise
 *
 * @return string
 */
function sanitise($data){
	// Ensures input is safe
	$data = trim($data);
	$data = urlencode($data);
	return $data;
}

$orig = sanitise($_POST["orig"]);
$dest = sanitise($_POST["dest"]);
$rc = sanitise($_POST["rc"]);

if ($_POST["sr"] == "single"){
	$single = true;
} else {
	$single = false;
}

if (!empty($orig) && !empty($dest)) { add_journey($orig, $dest, $single, $rc); }

?>
<!DOCTYPE html>
<html lang="en-GB"><head>
	<meta charset="utf-8"/>
	<title>Rail rover comparer</title>
	<link rel="stylesheet" type="text/css" href="main.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
	<h1 class="centre">Rail rovers and rangers comparison tool</h1>
	<div class="centre">
		<button id="ins_button">Show Instructions</button>
		<div id="instructions">
			<p>Enter your journey plans, (a railcard can only be set on the 1st journey).<br />
			Standard class walk-up (i.e. not Advance) fares will be shown in the 1st table along with totals of the cheapest and most expensive.</p>
			<p>Any rangers, rovers or PTE fares valid at all of your selected stations will be shown in the 2nd table, colour coded according to price:</p>
			<ul>
				<li>Green: Cheaper than any combination of walk-up singles and returns</li>
				<li>Yellow: Between the cheapest and most expensive combination of walk-up singles and returns</li>
				<li>Red: More expensive than any combination walk-up singles and returns</li>
			</ul>
			<p>Please check the restriction codes as peak time restrictions often vary between the ticket types.</p>
			<p>If a railcard is selected non-discounted rovers will also be shown as some of them do not allow discounts.</p>
		</div>
	</div>
	<script src="expand.js"></script>
<?php
require("form.php");

if (!empty($_SESSION["journeys"])){
	echo "<div id=\"results\">";
	display_fares($_SESSION["journeys"], $min, $max); // Display saved fares
	$valid = check_rovers($_SESSION["stations"], $_SESSION["disc"]);
	display_rovers($valid, $min, $max);
	echo "</div>";
}
?>
<p>N.B. This tool currently supports only a selection of rovers and PTE fares in Merseyside, Greater Manchester, Lancashire and Cumbria<br />Site &copy; Mark Wane <a href="https://gitlab.com/MAWane1/rail">Source</a>. Fares data &copy; <a href="http://www.atoc.org/">RSP</a>, provided by <a href="http://www.brfares.com/#home" target="_blank">BR Fares</a></p>
</body></html>

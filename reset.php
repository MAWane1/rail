<?php
/**
 * Resets session and redirects back to main page.
 */
session_start();
session_unset();
session_destroy();
header("Location: index.php");
?> 

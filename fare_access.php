<?php
/*
 * Copyright 2015-2016 Mark Wane
 *
 * This file is part of Rail rovers and rangers comparison tool.
 *
 * Rail rovers and rangers comparison tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rail rovers and rangers comparison tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Rail rovers and rangers comparison tool.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Allows usort to order fares by price
 */
function sort_price($a, $b) { // Sort fares from cheapest to most expensive
	if ($a->price == $b->price){ return 0; }
	return ($a->price < $b->price) ? -1 : 1;
}

/**
 * Checks if location is a station group
 *
 * @param object $location location object to test
 *
 * @return bool
 */
function find_stns($location) { // Returns true for station groups
	if (preg_match("/STATIONS$/", $location->value)){
		return true;
	} else {
		return false;
	}
}

/**
 * Processes journey
 *
 * @param string $orig origin station
 *
 * @param string $dest destination station
 *
 * @param bool $single if journey is one way
 *
 * @param string $rc Railcard to be used
 *
 * @return
 */
function add_journey($orig, $dest, $single, $rc){
	$journeys = $_SESSION["journeys"];
	$stations = $_SESSION["stations"];
	$disc = $_SESSION["disc"];

	// Converts locations from strings to NRS/NLC code and full name
	$orig = resolve_location($orig);
	$dest = resolve_location($dest);

	if (empty($disc)){ // Skip when discount already set
		if (!empty($rc)) { // Convert string to railcard code and name
			$disc = resolve_discount($rc);
		} else { // Default to public
			$disc->code = "   ";
			$disc->name = "PUBLIC";
		}
	}
	// Retrieve fares data
	$fares = access_fares($orig->code, $dest->code, $single, $disc->code);
	array_push($journeys, (object) array("orig" => $orig, "dest" => $dest, "single" => $single, "fares" => $fares));
	// Save new stations
	$ns = 0; // number of new stations
	if (!in_array($orig->code, $stations)) {
		array_push($stations, $orig->code);
		$ns++;
	}
	if (!in_array($dest->code, $stations)) {
		array_push($stations, $dest->code);
		$ns++;
	}
	// Save session
	$_SESSION["journeys"] = $journeys;
	$_SESSION["stations"] = $stations;
	$_SESSION["disc"] = $disc;
	array_push($_SESSION["undo"], $ns);
}

/**
 * Converts user input to location codes and full names
 *
 * @param string $location form input to process
 *
 * @return object
 */
function resolve_location($location) {
	$url = "http://api.brfares.com/ac_loc?term=".$location;

	$conn = curl_init($url);
	curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($conn, CURLOPT_ENCODING, "gzip");
	$result = curl_exec($conn);
	curl_close($conn);

	$list = json_decode($result);

	if (strlen($location)===3) { // assume code entered
		return $list[0];
	} else {
		$stns = array_values(array_filter($list, find_stns)); // find fares to station groups
		if (empty($stns)) {
			return $list[0]; // 1st result alphabetically 
		} else {
			return $stns[0]; // station group
		}
	}
}

/**
 * Converts user input to railcard code and name
 *
 * @param string $rc form input to process
 *
 * @return object
 */
function resolve_discount($rc){ // Converts user input to railcard code and name
	$url = "http://api.brfares.com/ac_rlc?term=".$rc;

	$conn = curl_init($url);
	curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($conn, CURLOPT_ENCODING, "gzip");
	$result = curl_exec($conn);
	curl_close($conn);

	$disc = json_decode($result);

	return $disc[0];
}

/**
 * Accesses fares for journey
 *
 * Reterives list of fares between the two stations with the railcard if chosen.
 * Then filters them to remove non-public, 1st class and advance fares
 *
 * @param string $orig CRS code for origin station
 *
 * @param string $dest CRS code for destination station
 *
 * @param bool $single if this journey is one way
 *
 * @param string $disc code for railcard (if used)
 *
 * @return array
 */
function access_fares($orig, $dest, $single, $disc) {
	$url = "http://api.brfares.com/querysimple?orig=".$orig."&dest=".$dest."&rlc=".$disc;

	$conn = curl_init($url);
	curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($conn, CURLOPT_ENCODING, "gzip");
	$result = curl_exec($conn);
	curl_close($conn);

	$flow = json_decode($result);

	$fares = array();
	foreach ($flow->fares as $fare) {
		if ($fare->category->code == 0){ // Walkup fares
			if ($fare->ticket->tclass->code == 1) { // Standard class
				if ($fare->adult->fare != 0 ) { // Exclude child only fares (usually add-ons)
					if (strpos($fare->ticket->longname, "CARNET") === false && strpos($fare->ticket->longname, "TAKE") === false && strpos($fare->ticket->longname, "GROUP") === false && strpos($fare->ticket->longname, "DUO") === false) { // Exclude carnet and group tickets
						if ($single) {
							if ($fare->ticket->type->code === 0){ // Single
								if ($fare->ticket->code != "SVH") { // Exclude saver-half
									$ticket = (object) array("type"=>$fare->ticket->longname, "route" => $fare->route->name, "price" => $fare->adult->fare, "restriction" => $fare->restriction_code);
									array_push($fares, $ticket);
								}
							} 
						} else {
							if ($fare->ticket->type->code === 1) { // Return
								if (!$fare->travelcard_dest && !$fare->travelcard_orig) { // Exclude travelcards
									$ticket = (object) array("type"=>$fare->ticket->longname, "route" => $fare->route->name, "price" => $fare->adult->fare, "restriction" => $fare->restriction_code);
									array_push($fares, $ticket);
								}
							}
						}
					}
				}
			} 
		}
	}

	usort($fares, sort_price);
	return $fares;
}

/**
 * Loads rovers files and selects rovers vaild for set of stations ordered by price
 *
 * @param array $stations list of stations used
 *
 * @param object $disc railcard if used
 *
 * @return array
 */
function check_rovers($stations, $disc){
	$rovers = json_decode(file_get_contents("rovers.json")); // Load file created by update.php
	$valid = array();

	foreach ($rovers as $rover) {
		if (!array_diff($stations, $rover->stations)) { // Rover is valid for all selected stations
			if ($rover->disc == "   ") { // Undiscounted, always shown as some rovers (e.g. PTE products) do not allow railcards
				array_push($valid, $rover);
			} if ($disc->code != "   ") { // Railcard selected
				if ($rover->disc == $disc->code) { // Railcard fare
					array_push($valid, $rover);
				}
			}
		}
	}

	usort($valid, sort_price);
	return $valid;
}
?>

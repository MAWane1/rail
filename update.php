<?php
/*
 * Copyright 2015-2016 Mark Wane
 *
 * This file is part of Rail rovers and rangers comparison tool.
 *
 * Rail rovers and rangers comparison tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rail rovers and rangers comparison tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Rail rovers and rangers comparison tool.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Update instructions:
 * 1. Download fares feed from http://data.atoc.org/data-download
 * 2. Extract file ending .TRR and rename to Rovers.TRR
 * 3. Run this script to update rover list
 */

/**
 * Updates rovers.json MUST be run from cli
 */
$rovers = array();
$farefile = fopen("Rovers.TRR", "r"); // Load rover file from fares database
$now = new DateTime();

while (($line = fgets($farefile)) !== false) {
	if (substr($line, 0, 1) === "P") { // Price records
		if (date_create_from_format("dmY", substr($line, 4, 8)) > $now) { // Not expired
			$rover["code"] = substr($line, 1, 3);
			$rover["disc"] = substr($line, 12, 3);
			$rover["price"] = substr($line, 16, 8);
			$rover["restriction"] = substr($line, 32, 2);
			array_push($rovers, $rover);
		}
	}
}
fclose($farefile);

$rstations = json_decode(file_get_contents("stations.json"), true); // Load rover names and lists of stations
$tmp = array();

foreach ($rstations as $rstation) {
	foreach($rovers as $rover){
		if ($rover["code"] === $rstation["code"]) { // Combine data sources
			array_push($tmp ,array_merge($rover, $rstation));
		}
	}
}

file_put_contents("rovers.json", json_encode($tmp, JSON_PRETTY_PRINT)); // Save combined file for use by live site

echo "Update completed \n";

?>

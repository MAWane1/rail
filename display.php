<?php
/*
 * Copyright 2015-2016 Mark Wane
 *
 * This file is part of Rail rovers and rangers comparison tool.
 *
 * Rail rovers and rangers comparison tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rail rovers and rangers comparison tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Rail rovers and rangers comparison tool.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Generates table showing point to point fares and calculates minimum and maximun totals
 *
 * @param array $journeys array of Journey objects to display
 *
 * @param int $min minimum possible total of fares
 *
 * @param int $max maximum possible total of fares
 *
 * @return void
 */
function display_fares($journeys, &$min, &$max){
	echo "<h2 class=\"centre\">Journeys</h2>\n <table class=\"journeys centre\"><tr><th>Origin</th> <th>Destination</th> <th>Single/Return</th> <th>Fares</th></tr> \n";
	foreach($journeys as $journey){
		echo "\t <tr><td>".$journey->orig->value."</td><td>".$journey->dest->value."</td><td>"; if ($journey->single){echo "Single";} else {echo "Return";}
		echo "</td><td><table class=\"fares\"><tr><th>Type</th><th>Route</th><th>Price</th><th>Restriction</th></tr> \n";

		foreach ($journey->fares as $row) {
			echo "\t \t <tr><td>".$row->type."</td><td>".$row->route."</td><td>£".number_format($row->price/100 ,2)."</td><td>";
			if ($row->restriction != "  ") {echo "<a href=\"http://www.nationalrail.co.uk/".$row->restriction."\" target=\"_blank\" >".$row->restriction."</a>";}
			echo "</td></tr> \n";
		}
		$min += $journey->fares[0]->price;
		$max += end($journey->fares)->price;
		echo "</table></td></tr> \n";
	}
	echo "\t <tr><th>Totals</th><th>Min</th><th>Max</th></tr> \n
		<tr><td></td><td>£".number_format($min/100 ,2)."</td><td>£".number_format($max/100 ,2)."</td></tr></table> \n";

}

/**
 * Generates table showing vaild rovers coloured according to potental savings
 *
 * @param array $rovers array containing valid rover objects
 *
 * @param int $min minimum possible total of point to point fares
 *
 * @param int $max maximum possible total of point to point fares
 *
 * @return void
 */
function display_rovers($rovers, $min, $max){
	echo "<h2 class=\"centre\">Rovers</h2>\n <table class=\"centre\"><tr><th>Name</th> <th>Price</th> <th>Restriction</th> <th>Railcard</th></tr> \n";
	foreach ($rovers as $rover){
		if ($rover->price <= $min) {
			echo "\t <tr class=\"low-price\">";
		} elseif ($rover->price <= $max) {
			echo "\t <tr class=\"mid-price\">";
		} else {
			echo "\t <tr class=\"high-price\">";
		}
		echo "<td>".$rover->name."</td><td>£".number_format($rover->price/100, 2)."</td><td>";
		if ($rover->restriction != "  ") {echo "<a href=\"http://www.nationalrail.co.uk/".$rover->restriction."\" target=\"_blank\" >".$rover->restriction."</a>";}
		echo "<td>".$rover->disc."</td></tr> \n";
	}
	echo "</table>";
}
?>

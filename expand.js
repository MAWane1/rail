// Copyright 2015-2016 Mark Wane
// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3-or-later

document.getElementById('ins_button').onclick = function expand() {
	var e = document.getElementById('instructions');
	var b = document.getElementById('ins_button');
	if(e.style.display == 'block') {
		e.style.display = 'none';
		b.innerHTML = 'Show Instructions';
	}
	else {
		e.style.display = 'block';
		b.innerHTML = 'Hide Instructions';
	}
};
// @license-end

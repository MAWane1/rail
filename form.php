<?php
/*
 * Copyright 2015-2016 Mark Wane
 *
 * This file is part of Rail rovers and rangers comparison tool.
 *
 * Rail rovers and rangers comparison tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rail rovers and rangers comparison tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Rail rovers and rangers comparison tool.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Form to be included on main page
 */
?>
<div id="form" class="centre"><form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
	<label for="orig">Origin</label><input type="text" id="orig" name="orig" value="<?php echo $orig->value; ?>" /><br />
	<label for="dest">Destination</label><input type="text" id="dest" name="dest" value="<?php echo $dest->value; ?>" /><br />
	<label for="rc">Railcard</label><input type="text" id="rc" name="rc" <?php if (!empty($_SESSION["disc"])){ echo "value=\"".$_SESSION["disc"]->label."\" disabled";} ?> /><br />
	<div class="rb">
		<label for="single">Single</label><input type="radio" name="sr" id="single" value="single" <?php if($single){echo "checked";} ?> />
		<label for="return">Return</label><input type="radio" name="sr" id="return" value="return" <?php if(!$single){echo "checked";} ?> />
	</div><br />
	<label></label><input type="submit" name="add" value="Add" id="add" /><?php if (count($_SESSION["undo"])>1) { echo "<input type=\"submit\" formaction=\"undo.php\" value=\"Undo\" id=\"undo\" />"; } ?><input type="submit" formaction="reset.php" value="Reset" id="reset" />
</form></div>

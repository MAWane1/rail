<?php
/*
 * Copyright 2015-2016 Mark Wane
 *
 * This file is part of Rail rovers and rangers comparison tool.
 *
 * Rail rovers and rangers comparison tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rail rovers and rangers comparison tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Rail rovers and rangers comparison tool.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Removes the last added journey
 */
session_start();
if (!empty($_SESSION["undo"])){ // undo possible
	$ns = array_pop($_SESSION["undo"]);
	for ($count=0; $count < $ns; $count++){ // number of stations added last time
		array_pop($_SESSION["stations"]);
	}
	array_pop($_SESSION["journeys"]); // Last added journey
}
header("Location: index.php");
?>

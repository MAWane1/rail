# Rail rovers and rangers comparison tool

A web application for comparing ranger and rover fares with a set of walk-up singles and/or returns

## Usage

Stations can be entered into the journey plan by code or name. In the case of ambiguous names a station group will take priority, otherwise the 1st result alphabetically will be selected.
A railcard can only be selected with the 1st journey to prevent data inconsistency.
Standard class walk-up (i.e. not Advance) fares will be shown in the 1st table along with totals of the cheapest and most expensive.

Any rangers, rovers or PTE fares valid at all of your selected stations will be shown in the 2nd table, colour coded according to price:

* Green: Cheaper than any combination of walk-up singles and returns
* Yellow: Between the cheapest and most expensive combination of walk-up singles and returns
* Red: More expensive than any combination walk-up singles and returns

It is important to check the restriction codes as peak time restrictions often vary between the ticket types.

N.B. If a railcard is selected non-discounted rovers will also be shown as some of them do not allow discounts.

## Installation / Updating

Single and return fares are obtained from the [BR Fares](http://www.brfares.com/#home) API and therefore should always be up to date.

Ranger and rover fares are not available in this way, requiring the data to be obtained from 2 other sources. 
Validity maps are only available in PDF form and must be manually converted into a usable form. This is the *stations.json* file which contains the fare's name and 3 character code along with an array of the 3 character station codes where it is valid. 
The price records can be obtained in the fares database from [ATOC](http://data.atoc.org/), the file ending .TRR should be extracted and renamed *Rovers.TRR*. 
Running *update.php* will combine the data from both files.
 
